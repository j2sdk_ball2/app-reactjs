const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const config = {
    context: path.resolve(__dirname, 'app/src'),
    entry: './app.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [

            {
                test: /\.jsx$/,
                include: path.resolve(__dirname, 'app/src'),
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ['react'],
                                ['es2015', { modules: false }]
                            ]
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            chunks: ['main']
        }),
    ],
    devServer: {
        contentBase: "./dist",
        port: 4500
    }
}

module.exports = config